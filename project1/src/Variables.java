
public class Variables {

	public static void main(String[] args) {
		
		int edad=35; //Declaracion e iniciacion de variable en �nica l�nea
		
		/*La siquiente instruccion imprime en la consola (System.out) el valor de
		 * la variable edad
		 */
		System.out.println(edad);
		
		edad=75;
		
		System.out.println(edad);
	}
	
}	
