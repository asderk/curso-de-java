package poo;

public class Prueba {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Empleados trabajador1=new Empleados("Paco");
		
		trabajador1.cambiaSeccion("RRHH");
		
		Empleados trabajador2=new Empleados("Ana");
		
		Empleados trabajador3=new Empleados("Ramiro");
		
		Empleados trabajador4=new Empleados("Jose");
		
		System.out.println(trabajador1.devuelveDatos());
	
		System.out.println(trabajador2.devuelveDatos());
		
		System.out.println(trabajador3.devuelveDatos());
		
		System.out.println(trabajador4.devuelveDatos());
		
	}

}


class Empleados{
	
	public Empleados(String nom) {
		
		nombre=nom;
		
		seccion="Administración";
		
	}
	
	public void cambiaSeccion(String seccion) { //SETTER
		
		this.seccion=seccion;		
	}
	
	public void cambiaNombre(String nombre) {
		

		
	}
	
	public String devuelveDatos() {
		
		return "El nombre es: " + nombre + " y la sección es " + seccion;
	}
	
	private final String nombre;
	
	private String seccion;
	
}