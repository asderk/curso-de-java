
public class Bi_arrays {

	public static void main(String[] args) {
		
		int[][] matrix= {
				
				{10,15,18,19,21},
				{29,33,68,1,8},
				{9,66,34,97,2},
				{1,2,3,4,5}
				
		};
		
		for (int[]fila:matrix) {
			
			System.out.println();
			
			for(int z: fila) {
				
				System.out.print(z + " ");
				
			}
			
		}
		
	}

}
